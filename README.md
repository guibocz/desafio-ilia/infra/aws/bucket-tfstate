# Bucket tfstate

## Purpose

That bucket was created to store the tfstate of the projects under [desafio-ilia](https://gitlab.com/guibocz/desafio-ilia), except itself

## Dependencies

The CI of this project depends on the following environment variables that should be defined in GitLab:

| Name                    | Value                              |
|-------------------------|------------------------------------|
| AWS_ACCESS_KEY_ID       | access key to aws account          |
| AWS_SECRET_ACCESS_KEY   | secret key to aws account          |
| TF_VAR_gitlab_api_token | gitlab api token (not in use yet)  |