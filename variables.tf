##############
### GITLAB ###
##############

variable "gitlab_api_token" {
  type = string
}

###########
### AWS ###
###########

variable "bucket_name" {
  type = string
}

variable "bucket_acl" {
  type = string
}