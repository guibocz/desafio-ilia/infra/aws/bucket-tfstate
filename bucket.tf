resource "aws_s3_bucket" "bucket-tfstate-ilia" {
  bucket = var.bucket_name
}

resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.bucket-tfstate-ilia.id
  acl    = var.bucket_acl
}