provider "aws" {
  region = "sa-east-1"
}

provider "gitlab" {
  base_url = "https://gitlab.com/api/v4/"
  token    = var.gitlab_api_token
  # source: https://medium.com/devops-with-valentine/how-to-use-the-gitlab-rest-api-ba4e4ca1fcae
}

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.9.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.13.0"
    }
    null = {
      source = "hashicorp/null"
    }
  }
  required_version = ">= 1.1.4"
}
